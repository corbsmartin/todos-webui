# =============================================================================
# Makefile variables
# =============================================================================
VERSION=1.0.15
TAG=v$(VERSION)
IMAGE_NAME=todos-webui
IMAGE_REPO=quay.io/corbsmartin/$(IMAGE_NAME)

image:
	@podman build --no-cache -t $(IMAGE_NAME):$(TAG) --build-arg=APP_VERSION=$(VERSION) .

push: image
	@podman tag $(IMAGE_NAME):$(TAG) $(IMAGE_REPO):$(TAG)
	@podman push $(IMAGE_REPO):$(TAG)

run:
	@podman run --name $(IMAGE_NAME) -d -p 8080:8080 $(IMAGE_NAME):$(TAG)

build:
	@mvn versions:set -DnewVersion=$(VERSION)
	@mvn clean package -DskipTests=true

tag-commit:
	@git tag $(TAG)
	@git push --atomic origin main $(TAG)